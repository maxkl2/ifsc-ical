This Rust server application provides a web calendar for [IFSC](https://www.ifsc-climbing.org/) competition events.

A reverse proxy such as nginx or similar is required to serve the static files in `public/`.
