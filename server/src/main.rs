mod cache;
mod config;

use std::collections::HashSet;
use std::error::Error;
use std::sync::Arc;
use std::time::Duration;
use chrono::Datelike;
use serde::{Deserialize, Deserializer};
use icalendar::Component;
use tokio::sync::Mutex as AsyncMutex;
use warp::{Filter, Reply};
use ifsc_calendar_api::IfscCalendar;
use crate::cache::{CalendarCache, IndexCache};
use crate::config::Config;

/// Deserializes a comma-separated list of league IDs
fn deserialize_league_id_list<'de, D>(deserializer: D) -> Result<Vec<i32>, D::Error>
    where
        D: Deserializer<'de>
{
    let s = String::deserialize(deserializer)?;
    Ok(s.split(",")
        .filter_map(|item| item.parse::<i32>().ok())
        .collect())
}

/// Deserializes a comma-separated list of league names
fn deserialize_league_name_list<'de, D>(deserializer: D) -> Result<Vec<String>, D::Error>
    where
        D: Deserializer<'de>
{
    let s = String::deserialize(deserializer)?;
    Ok(s.split(",")
        .map(String::from)
        .collect())
}

#[derive(Debug, Deserialize)]
struct QueryOptions {
    #[serde(default, deserialize_with = "deserialize_league_id_list")]
    leagues: Vec<i32>,
    #[serde(default, deserialize_with = "deserialize_league_name_list")]
    league_names: Vec<String>,
    name: Option<String>,
    force_plaintext: Option<String>
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let config = Config::read_file("config.toml")?;

    let index_cache = IndexCache::new(Duration::from_secs(config.cache.ttl));
    let index_cache_mutex = Arc::new(AsyncMutex::new(index_cache));

    let cal_cache = CalendarCache::new(Duration::from_secs(config.cache.ttl));
    let cal_cache_mutex = Arc::new(AsyncMutex::new(cal_cache));

    let index_cache_mutex_1 = Arc::clone(&index_cache_mutex);
    let index = warp::get()
        .and(warp::path("index.json"))
        .then(move || {
            let index_cache_local = Arc::clone(&index_cache_mutex_1);
            async move {
                let mut index_cache_locked = index_cache_local.lock()
                    .await;

                let index = match index_cache_locked.get().await {
                    Ok(index) => index,
                    Err(err) => return warp::reply::with_status(err.to_string(), warp::http::StatusCode::INTERNAL_SERVER_ERROR).into_response()
                };
                warp::reply::json(&index).into_response()
            }
        });

    let index_cache_mutex_2 = Arc::clone(&index_cache_mutex);
    let cal = warp::get()
        .and(warp::path("ifsc.ics"))
        .and(warp::query::<QueryOptions>())
        .then(move |options: QueryOptions| {
            let index_cache_local = Arc::clone(&index_cache_mutex_2);
            let cal_cache_local = Arc::clone(&cal_cache_mutex);
            async move {
                let mut requested_league_ids = options.leagues.clone();

                if !options.league_names.is_empty() {
                    let mut index_cache_locked = index_cache_local.lock()
                        .await;

                    let index = match index_cache_locked.get().await {
                        Ok(index) => index,
                        Err(err) => return warp::reply::with_status(err.to_string(), warp::http::StatusCode::INTERNAL_SERVER_ERROR).into_response()
                    };

                    let current_year = chrono::Utc::now().year();

                    let mut relevant_season_ids = HashSet::new();
                    for (_, season) in &index.seasons {
                        if let Ok(season_year) = season.name.parse::<i32>() {
                            if (season_year - current_year).abs() <= 1 {
                                relevant_season_ids.insert(season.id);
                            }
                        }
                    }

                    for league_name in &options.league_names {
                        for (_, league) in &index.leagues {
                            let season_id = league.parent_id;
                            if relevant_season_ids.contains(&season_id) {
                                if &league.name == league_name {
                                    requested_league_ids.push(league.id);
                                }
                            }
                        }
                    }
                }

                let mut cal_cache_locked = cal_cache_local.lock()
                    .await;

                let mut cal = IfscCalendar::empty();
                for league_id in requested_league_ids {
                    match cal_cache_locked.get(league_id).await {
                        Ok(league_cal) => cal.events.extend(league_cal.events.iter().cloned()),
                        Err(err) => eprintln!("Failed to fetch calendar for league {}: {}", league_id, err)
                    }
                }
                let ical = create_ical(&cal, &chrono::Duration::seconds(config.icalendar.ttl as i64), options.name.as_deref());
                let ical_text = ical.to_string();
                warp::reply::with_header(ical_text, "Content-Type", if options.force_plaintext.is_some() { "text/plain" } else { "text/calendar" })
                    .into_response()
            }
        });

    let api = index
        .or(cal);

    let routes = api;

    warp::serve(routes)
        .run((config.server.ip, config.server.port))
        .await;

    Ok(())
}

const EVENT_DETAILS_URL: &str = "https://www.ifsc-climbing.org/index.php/component/ifsc/?view=event&WetId=%EVENT_ID%";

fn create_ical(cal: &IfscCalendar, ttl: &chrono::Duration, name: Option<&str>) -> icalendar::Calendar {
    let mut ical = icalendar::Calendar::new();

    if let Some(name) = name {
        ical.name(name);
    }

    ical.ttl(ttl);

    for event in &cal.events {
        let details_url = EVENT_DETAILS_URL.replace("%EVENT_ID%", &event.event_id.to_string());
        let ical_event = icalendar::Event::new()
            .summary(&event.event)
            .description(&details_url)
            .starts(event.starts_at)
            .ends(event.ends_at)
            .add_property("TRANSP", "TRANSPARENT") // Set event as "not busy"
            .done();
        ical.push(ical_event);
    }

    ical.done()
}
