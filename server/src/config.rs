use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::net::IpAddr;
use std::path::Path;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub server: ServerConfig,
    pub cache: CacheConfig,
    pub icalendar: ICalendarConfig
}

#[derive(Deserialize)]
pub struct ServerConfig {
    pub ip: IpAddr,
    pub port: u16
}

#[derive(Deserialize)]
pub struct CacheConfig {
    pub ttl: u64
}

#[derive(Deserialize)]
pub struct ICalendarConfig {
    pub ttl: u64
}

impl Config {
    pub fn read_file<P: AsRef<Path>>(path: P) -> Result<Config, Box<dyn Error>> {
        let mut f = File::open(path)?;
        let mut s = String::new();
        f.read_to_string(&mut s)?;
        let config = toml::from_str(&s)?;
        Ok(config)
    }
}
