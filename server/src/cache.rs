use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::time::{Duration, Instant};
use ifsc_calendar_api::{IfscCalendar, IfscIndex};

struct IndexCacheItem {
    timestamp: Instant,
    value: IfscIndex
}

/// Caches index for the specified TTL
pub struct IndexCache {
    index: Option<IndexCacheItem>,
    ttl: Duration
}

impl IndexCache {
    pub fn new(ttl: Duration) -> IndexCache {
        IndexCache {
            index: None,
            ttl
        }
    }

    /// CLear/invalidate the whole cache
    pub fn clear(&mut self) {
        self.index = None;
    }

    /// Invalidate a single calendar
    pub fn invalidate(&mut self) {
        self.index = None;
    }

    async fn retrieve() -> Result<IndexCacheItem, ifsc_calendar_api::reqwest::Error> {
        let value = IfscIndex::retrieve()
            .await?;

        Ok(IndexCacheItem {
            timestamp: Instant::now(),
            value
        })
    }

    /// Look up the index in the cache. If it is cached and not yet expired, it's immediately returned. Otherwise, the index is retrieved from the IFSC API.
    pub async fn get(&mut self) -> Result<&IfscIndex, ifsc_calendar_api::reqwest::Error> {
        let need_to_retrieve = match &self.index {
            Some(item) => item.timestamp.elapsed() > self.ttl,
            None => true
        };

        if need_to_retrieve {
            let item = Self::retrieve()
                .await?;

            Ok(&self.index.insert(item).value)
        } else {
            Ok(&self.index.as_ref().unwrap().value)
        }
    }
}

struct CalendarCacheItem {
    timestamp: Instant,
    value: IfscCalendar
}

/// Caches calendars for the specified TTL
pub struct CalendarCache {
    items: HashMap<i32, CalendarCacheItem>,
    ttl: Duration
}

impl CalendarCache {
    pub fn new(ttl: Duration) -> CalendarCache {
        CalendarCache {
            items: HashMap::new(),
            ttl
        }
    }

    /// CLear/invalidate the whole cache
    pub fn clear(&mut self) {
        self.items.clear();
    }

    /// Invalidate a single calendar
    pub fn invalidate(&mut self, key: i32) {
        self.items.remove(&key);
    }

    async fn retrieve(key: i32) -> Result<CalendarCacheItem, ifsc_calendar_api::reqwest::Error> {
        let value = IfscCalendar::retrieve(key)
            .await?;

        Ok(CalendarCacheItem {
            timestamp: Instant::now(),
            value
        })
    }

    /// Look up a calendar in the cache. If it is cached and not yet expired, it's immediately returned. Otherwise, the calendar is retrieved from the IFSC API.
    pub async fn get(&mut self, key: i32) -> Result<&IfscCalendar, ifsc_calendar_api::reqwest::Error> {
        match self.items.entry(key) {
            Entry::Occupied(mut entry) => {
                let item = entry.get();

                if item.timestamp.elapsed() > self.ttl {
                    let item = Self::retrieve(key)
                        .await?;

                    entry.insert(item);
                }

                let item = entry.into_mut();

                Ok(&item.value)
            }
            Entry::Vacant(entry) => {
                let item = Self::retrieve(key)
                    .await?;

                let item = entry.insert(item);

                Ok(&item.value)
            }
        }
    }
}
