(async () => {
    const SERVER_ROOT = 'https://projects.maxkl.de/ifsc-ical';

    let leaguesSelect = document.querySelector('#select-leagues');
    let nameInput = document.querySelector('#input-name');
    let linkInput = document.querySelector('#input-link');
    let copyLinkBtn = document.querySelector('#btn-copy-link');
    let copyLinkSpan = document.querySelector('#span-copy-link');

    let index = await fetch(SERVER_ROOT + '/index.json')
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch index');
            }

            return response.json();
        })
        .catch(err => {
            let statusOption = leaguesSelect.firstElementChild;
            statusOption.textContent = 'Failed to load leagues';
            statusOption.classList.add('text-danger');
            throw err;
        });

    let leagueNames = new Set();
    for (let league of Object.values(index.leagues)) {
        leagueNames.add(league.name);
    }

    let sortedLeagueNames = Array.from(leagueNames).sort();
    leaguesSelect.replaceChildren();
    for (let leagueName of sortedLeagueNames) {
        let option = document.createElement('option');
        option.value = leagueName;
        option.text = leagueName;
        leaguesSelect.appendChild(option);
    }
    leaguesSelect.size = sortedLeagueNames.length;

    function updateLink() {
        let selected = Array.from(leaguesSelect.selectedOptions)
            .map(option => option.value);

        let selectedEscaped = selected.map(encodeURIComponent);
        let nameEscaped = encodeURIComponent(nameInput.value);
        linkInput.value = `${SERVER_ROOT}/ifsc.ics?league_names=${selectedEscaped.join(',')}&name=${nameEscaped}`;

        if (selected.length > 0) {
            copyLinkBtn.removeAttribute('disabled');
        } else {
            copyLinkBtn.setAttribute('disabled', true);
        }
    }

    function copyTextInputFallback(input) {
        input.focus();
        input.select();
        try {
            let successful = document.execCommand('copy');
            if (successful) {
                return Promise.resolve();
            } else {
                return Promise.reject('document.execCommand(\'copy\') failed');
            }
        } catch (err) {
            return Promise.reject(err);
        }
    }

    function copyTextInput(input) {
        input.focus();
        input.select();
        if (!navigator.clipboard) {
            return copyTextInputFallback(input);
        }
        return navigator.clipboard.writeText(input.value);
    }

    function copyLink() {
        copyTextInput(linkInput)
            .then(() => {
                copyLinkBtn.classList.remove('btn-primary');
                copyLinkBtn.classList.add('btn-success');
                copyLinkSpan.classList.remove('bi-clipboard');
                copyLinkSpan.classList.add('bi-clipboard-check');
            }, err => {
                console.error(err);
                copyLinkBtn.classList.remove('btn-primary');
                copyLinkBtn.classList.add('btn-danger');
                copyLinkSpan.classList.remove('bi-clipboard');
                copyLinkSpan.classList.add('bi-clipboard-x');
            });
    }

    function restoreCopyButton() {
        copyLinkBtn.classList.remove('btn-success', 'btn-danger');
        copyLinkBtn.classList.add('btn-primary');
        copyLinkSpan.classList.remove('bi-clipboard-check', 'bi-clipboard-x');
        copyLinkSpan.classList.add('bi-clipboard');
    }

    leaguesSelect.addEventListener('input', () => updateLink(true));
    nameInput.addEventListener('input', () => updateLink(false));
    copyLinkBtn.addEventListener('click', copyLink);
    copyLinkBtn.addEventListener('mouseleave', restoreCopyButton);

    updateLink();
})();